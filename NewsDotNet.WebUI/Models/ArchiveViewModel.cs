﻿using System.Collections.Generic;

namespace NewsDotNet.WebUI.Models
{
    public class ArchiveViewModel
    {
        public IEnumerable<ArchiveArticles> Days { get; set; }
        public PagingInfo PagingInfo { get; set; }
    }
}