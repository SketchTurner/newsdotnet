﻿var editor;
var form;
var alertBox;
var lockerMessageContainer;
var isFormDetached = false;
var isCKEditorReady = CKEDITOR.instances.length > 0;
var isFormHidden = false;

onCKEditorInstanceReady = function (event) {
    isCKEditorReady = true;
    if (isFormHidden) {
        detachForm();
    }
}

function initializeCKEditor() {
    editor = CKEDITOR.instances['Body'];
    if (editor) { editor.destroy(true); }

    editor = CKEDITOR.replace('Body', {
        enterMode: CKEDITOR.ENTER_BR,
        language: 'ru',
        on: {
            instanceReady: onCKEditorInstanceReady
        }
    });
}

initializeCKEditor();

$(document).ready(function () {
    console.log('Doc ready\t\t' + Date.now() / 1000 % 86400);
    var timerId = -1;
    var articleId = $('#Id').val();
    alertBox = $("#alert-box");
    lockerMessageContainer = $("#locker-message");

    if (lockerMessageContainer.text()) {
        showLocker(lockerMessageContainer.text());
    }

    var articles = $.connection.articleEditorHub;

    $.connection.hub.start().done(function () {
        console.log('Started\t' + Date.now() / 1000 % 86400);
        articles.server.isLocked(articleId);
        console.log('Request sent\t' + Date.now() / 1000 % 86400);
    });

    articles.client.notifyLocked = function (msg) {
        console.log('Result received\t' + Date.now() / 1000 % 86400);
        if (timerId == -1) {
            showLocker(msg);
        }
        timerId = setTimeout(function () { articles.server.isLocked(articleId) }, 5000);
    }

    articles.client.notifyUnlocked = function (newArticle) {
        if (timerId != -1) {
            hideLocker(newArticle);
            clearInterval(timerId);
            timerId = -1;
        }
    }
});

function showLocker(msg) {
    if (!isFormDetached) {
        $('form').fadeOut();
        isFormHidden = true;

        if (isCKEditorReady) {
            detachForm();
        }
        lockerMessageContainer.text(msg);
        alertBox.addClass('alert alert-warning');
        alertBox.fadeIn();
        console.log('Form detached\t' + Date.now() / 1000 % 86400);
    }
}

function hideLocker(newArticle) {
    if (isFormDetached) {
        form.insertAfter(alertBox);
        isFormDetached = false;
        setNewFormValues(newArticle);
        alertBox.fadeOut().removeClass('alert alert-warning');
        $('form').fadeIn();
        isFormHidden = false;
    }
}

function setNewFormValues(newArticle) {
    $('#State').val(newArticle.State);
    $('#Title').val(newArticle.Title);
    $('#AddressName').val(newArticle.AddressName);
    $('#Body').val(newArticle.Body);
    $('#TitleImagePath').val(newArticle.TitleImagePath);
    $('#TagsString').importTags(newArticle.Tags.map(function (tag) { return tag.Name }).join());
    setNewImagePath(newArticle.TitleImagePath);
    initializeCKEditor();
}

function setNewImagePath(filePath) {
    var oldPath = $('img#uploadPreview').attr('src');
    var directory = oldPath.substring(0, oldPath.lastIndexOf('/') + 1);
    var fileName = filePath.substring(filePath.lastIndexOf('/') + 1);
    $('img#uploadPreview').attr('src', directory + fileName);
}

function detachForm() {
    editor.destroy();
    form = $('form').detach();
    isFormDetached = true;
}