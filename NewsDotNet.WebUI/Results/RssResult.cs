﻿using System.Xml;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.ServiceModel.Syndication;

namespace NewsDotNet.WebUI.Results
{
    public class RssResult : FileResult
    {
        private readonly SyndicationFeed _feed;

        public RssResult(SyndicationFeed feed) : base("application/rss+xml")
        {
            _feed = feed;
        }

        public RssResult(string title, List<SyndicationItem> feedItems) : base("application/rss+xml")
        {
            _feed = new SyndicationFeed(title, title, HttpContext.Current.Request.Url) { Items = feedItems };
        }

        protected override void WriteFile(HttpResponseBase response)
        {
            using (var writer = XmlWriter.Create(response.OutputStream))
            {
                _feed.GetRss20Formatter().WriteTo(writer);
            }
        }
    }
}