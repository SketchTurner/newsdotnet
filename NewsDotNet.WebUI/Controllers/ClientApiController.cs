﻿using NewsDotNet.DomainModel.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity.Owin;
using NewsDotNet.WebUI.Infrastracture;
using Microsoft.AspNet.Identity;
using NewsDotNet.DomainModel.Entities;
using System.Web.Http;

namespace NewsDotNet.WebUI.Controllers
{
    public class ClientApiController : ApiController
    {
        private IArticlesRepository _articleRepo;
        private IMainPageEntitiesRepository _mainPageRepo;

        public ClientApiController(IArticlesRepository articleRepo, IMainPageEntitiesRepository mainPageRepo)
        {
            _articleRepo = articleRepo;
            _mainPageRepo = mainPageRepo;
        }

        [HttpGet]
        public dynamic MainPageArticles()
        {
            var articles = from article in _mainPageRepo.All().OrderBy(a => a.GridData.Row).Select(a => a.Article).ToList()
                           select new
                           {
                               article.AddressName,
                               article.Title,
                               article.TitleImagePath,
                               Author = new
                               {
                                   Name = GetAuthorName(article.AuthorId),
                                   Login = GetAuthorLogin(article.AuthorId)
                               },
                               Tags = from tag in article.Tags select tag.Name,
                               Time = FormatDate(article.LastChangedTime)
                           };
            return articles;
        }

        [HttpGet]
        public dynamic ArchiveArticles()
        {
            var articles = from article in GetArchiveArticles()
                           select new
                           {
                               article.AddressName,
                               article.Title,
                               Author = new
                               {
                                   Name = GetAuthorName(article.AuthorId),
                                   Login = GetAuthorLogin(article.AuthorId)
                               },
                               Tags = from tag in article.Tags select tag.Name,
                               Time = FormatDate(article.LastChangedTime)
                           };
            return articles.ToList();
        }

        public string Article(string addressName)
        {
            var article = _articleRepo.GetByAddressName(addressName);
            return ConvertArticleToJson(article);
        }

        public string ArticleById(int id)
        {
            var article = _articleRepo.GetById(id);
            return ConvertArticleToJson(article);
        }

        private string ConvertArticleToJson(Article article)
        {
            dynamic result;
            if (null == article)
                result = new { result = "error", msg = "Не удалось найти статью" };
            else
                result = new
                {
                    result = "success",
                    article = new
                    {
                        article.AddressName,
                        article.Title,
                        article.TitleImagePath,
                        Author = new
                        {
                            Name = GetAuthorName(article.AuthorId),
                            Login = GetAuthorLogin(article.AuthorId)
                        },
                        Body = ParserSingletone.ToHtml(article.Body),
                        Tags = from tag in article.Tags select tag.Name,
                        Time = FormatDate(article.LastChangedTime)
                    }
                };
            return Json(result);
        }

        private string GetAuthorName(string id)
        {
            var author = GetAuthor(id);
            if (author == null)
                return "Author";
            return String.Join(" ", author.FirstName, author.LastName);
        }

        private string GetAuthorLogin(string id)
        {
            var author = GetAuthor(id);
            if (author == null)
                return "admin";
            return author.UserName;
        }

        private User GetAuthor(string id)
        {
            var userManager = HttpContext.Current.GetOwinContext().GetUserManager<IdentityUserManager>();
            return userManager.FindById(id);
        }

        private string FormatDate(DateTime date)
        {
            return date.ToString("f", new System.Globalization.CultureInfo("ru-RU"));
        }

        private IEnumerable<Article> GetArchiveArticles()
        {
            return from article in _articleRepo.All()
                   join mainPageEntity in _mainPageRepo.All()
                   on article.Id equals mainPageEntity.ArticleID into t
                   from mpe in t.DefaultIfEmpty()
                   where mpe == null && article.State == ArticleStates.Published
                   orderby article.CreatedTime descending
                   select article;
        }
    }
}