﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using NewsDotNet.DomainModel.Abstract;

namespace NewsDotNet.WebUI.Hubs
{
    [Authorize]
    public class ArticleEditorHub : Hub
    {
        //  Key - article's ID, Value - editor's connectionId
        private static Dictionary<int, string> currentlyEditedArticles = new Dictionary<int, string>();
        
        //  Key - connectionId, Value - UserName
        private static Dictionary<string, string> users = new Dictionary<string, string>();

        private IArticlesRepository _articleRepo;

        public ArticleEditorHub(IArticlesRepository articleRepo)
        {
            _articleRepo = articleRepo;
        }

        public override Task OnConnected()
        {
            System.Diagnostics.Debug.WriteLine(DateTime.Now);
            string connectionId = Context.ConnectionId;
            if (!users.Keys.Contains(connectionId))
            {
                users.Add(connectionId, Context.User.Identity.Name);
            }
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            string connectionId = Context.ConnectionId;
            users.Remove(connectionId);
            UnlockArticle(connectionId);
            return base.OnDisconnected(stopCalled);
        }

        public void IsLocked(int articleId)
        {
            string connectionId = Context.ConnectionId;
            bool isEditedByYou;
            string message = GetLockerMessage(articleId, Context.User.Identity.Name, out isEditedByYou);

            if (message != null)
            {
                if (currentlyEditedArticles[articleId] != Context.ConnectionId)
                {
                    Clients.Caller.notifyLocked(message);
                    return;
                }
            }
            else
            {
                currentlyEditedArticles.Add(articleId, connectionId);
            }

            var article = _articleRepo.GetById(articleId);
            System.Diagnostics.Debug.WriteLine(DateTime.Now);
            Clients.Caller.notifyUnlocked(article);
        }

        private void UnlockArticle(string connectionId)
        {
            int articleId = currentlyEditedArticles.FirstOrDefault(a => a.Value == connectionId).Key;
            currentlyEditedArticles.Remove(articleId);
        }

        public static string GetLockerMessage(int articleId, string userName, out bool isEditedByYou)
        {
            string message = null;
            isEditedByYou = false;

            if (currentlyEditedArticles.Keys.Contains(articleId))
            {
                string editorName = users[currentlyEditedArticles[articleId]];

                if (editorName == userName)
                {
                    message = "Данная статья уже редактируется Вами в другом окне";
                    isEditedByYou = true;
                }
                else
                {
                    message = String.Format("Данная статья уже редактируется пользователем {0}", editorName);
                }
            }

            return message;
        }
    }
}