﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NewsDotNet.DomainModel.Abstract;
using NewsDotNet.DomainModel.Entities;
using NewsDotNet.WebUI.Controllers;

namespace NewsDotNet.WebUI.Tests
{
    [TestClass]
    public class HomeController_Test
    {
        [TestMethod]
        public void Index_Returns_Published_Articles()
        {
            Mock<IMainPageEntitiesRepository> mock = new Mock<IMainPageEntitiesRepository>();
            mock.Setup(m => m.All()).Returns(new MainPageEntity[]
            {
                new MainPageEntity() {Article = new Article() {AddressName = "1", State = ArticleStates.Published}},
                new MainPageEntity() {Article = new Article() {AddressName = "2", State = ArticleStates.Draft}},
                new MainPageEntity() {Article = new Article() {AddressName = "3", State = ArticleStates.Draft}}
            });

            var controller = new HomeController(mock.Object);
            var entities = (controller.Index() as ViewResult)?.Model as List<MainPageEntity>;

            Assert.IsNotNull(entities);
            Assert.IsTrue(entities.Count == 1);
            Assert.IsTrue(entities[0].Article.AddressName.Equals("1"));
        }

        [TestMethod]
        public void Index_Does_Not_Return_Not_Published_Articles()
        {
            Mock<IMainPageEntitiesRepository> mock = new Mock<IMainPageEntitiesRepository>();
            mock.Setup(m => m.All()).Returns(new MainPageEntity[]
            {
                new MainPageEntity() {Article = new Article() {AddressName = "1", State = ArticleStates.Draft}},
                new MainPageEntity() {Article = new Article() {AddressName = "1", State = ArticleStates.Draft}}
            });

            var controller = new HomeController(mock.Object);
            var entities = (controller.Index() as ViewResult)?.Model as List<MainPageEntity>;

            Assert.IsNotNull(entities);
            Assert.IsTrue(entities.Count == 0);
        }
    }
}
